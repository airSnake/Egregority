package e.timoshii.egregority;

/**
 * Created by timoshii on 11/03/2018.
 */

public class Message {

    private String content;
    private String username;
    private String time;

    public Message() { }

    public Message(String content, String username, String time)

    {
        this.content = content;
        this.username = username;
        this.time = time;
    }

    //content
    public String getContent()
    {
        return content;
    }

    public void setContent()
    {
        this.content = content;
    }

    //username
    public String getUsername()
    {
        return username;
    }

    public void setUsername()
    {
        this.username = username;
    }

    //timestamp
    public String getTime() {return time;}

    public void setTime() {this.time = time; }
}
