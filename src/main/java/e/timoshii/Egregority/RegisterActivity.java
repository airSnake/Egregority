package e.timoshii.egregority;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;



public class RegisterActivity extends AppCompatActivity {

    private EditText email;
    private EditText username;
    private EditText password;
    private EditText authority;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        username = findViewById(R.id.userNameLogin);
        email = findViewById(R.id.userEmailLogin);
        password = findViewById(R.id.userPasswordLogin);
        //authority = findViewById(R.id.authority);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mAuth = FirebaseAuth.getInstance();
    }

    public void RegisterButtonClicked(View view)
    {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        final String usernameC = username.getText().toString().trim();
        final String emailC = email.getText().toString().trim();
        final String passwordC = password.getText().toString().trim();

        if (!TextUtils.isEmpty(usernameC) && !TextUtils.isEmpty(emailC) && !TextUtils.isEmpty(passwordC))
        {
            mAuth.createUserWithEmailAndPassword(emailC,passwordC).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task)
                {
                    if(task.isSuccessful())
                    {
                        String userID = mAuth.getCurrentUser().getUid();
                        DatabaseReference current_user_db = mDatabase.child(userID);
                        current_user_db.child("Name").setValue(usernameC);
                        current_user_db.child("l/0").setValue(0);
                        current_user_db.child("l/1").setValue(1);
                        startActivity(new Intent(RegisterActivity.this,MainActivity.class));
                    }
                }
            });
        }
    }

    public void LoginButtonCLicked(View view)
    {
        startActivity(new Intent(RegisterActivity.this,MainActivity.class));
    }
}


