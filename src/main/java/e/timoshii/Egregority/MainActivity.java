package e.timoshii.egregority;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends Activity {
    //User details
    private EditText username;
    private EditText email;
    private EditText password;

    //Database
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    //listens to database for changes - need to control this between activities
    ValueEventListener mdbl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        username = findViewById(R.id.userNameLogin);
        email = findViewById(R.id.userEmailLogin);
        password = findViewById(R.id.userPasswordLogin);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

    }

    public void loginButtonClicked(View view)
    { //parses input to send to database
        final String usernameC = username.getText().toString().trim();
        final String emailC = email.getText().toString().trim();
        final String passwordC = password.getText().toString().trim();

        //checks if
        if (!TextUtils.isEmpty(usernameC) && !TextUtils.isEmpty(emailC) && !TextUtils.isEmpty(passwordC))
        {
            mAuth.signInWithEmailAndPassword(emailC,passwordC).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful())
                    {
                        UserCheck();
                    }
                }
            });
        }
    }

    public void RegisterButtonClicked(View view)
    {
        Intent registerIntent = new Intent(MainActivity.this, RegisterActivity.class);
        startActivity(registerIntent);
    }


    public void UserCheck()
    {
        final String userID = mAuth.getCurrentUser().getUid();
        mdbl = new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.hasChild(userID))
                {
                    Intent chatIntent = new Intent(MainActivity.this, ChatActivity.class);
                    startActivity(chatIntent);
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Toast.makeText(getApplicationContext(), "Login Failed",  Toast.LENGTH_LONG).show();
            }
        };
        mDatabase.addValueEventListener(mdbl);
    }
    // prevents activity-jumping because database!
    @Override
    protected void onPause()
    {
        super.onPause();

        mDatabase.removeEventListener(mdbl);
    }
}

