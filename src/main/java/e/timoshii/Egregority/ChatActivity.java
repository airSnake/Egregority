package e.timoshii.egregority;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ChatActivity extends AppCompatActivity {



    private EditText editMessage;
    private DatabaseReference mDatabase;
    private RecyclerView messageList;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mDatabaseUsers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_main);

        editMessage = findViewById(R.id.editMessageI);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Messages");
        messageList = findViewById(R.id.messageRecyc);
        messageList.setHasFixedSize(true);
        messageList = findViewById(R.id.messageRecyc);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        messageList.setLayoutManager(linearLayoutManager);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null)
                {
                    startActivity(new Intent(ChatActivity.this,RegisterActivity.class));
                }
            }
        };
    }


    public void sendButtonClicked(View view)
    {
        mCurrentUser = mAuth.getCurrentUser();
        mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("Users").child(mCurrentUser.getUid());
        final String messageContent = editMessage.getText().toString().trim();
        SimpleDateFormat s = new SimpleDateFormat("dd/MM [hh:mm]");

        final String messageTime = s.format(new Date());
        if (!TextUtils.isEmpty(messageContent))
        {
           final DatabaseReference post = mDatabase.push();
           mDatabaseUsers.addValueEventListener(new ValueEventListener() {
               @Override
               public void onDataChange(DataSnapshot dataSnapshot) {
                   post.child("time").setValue( messageTime );
                   post.child("content").setValue(messageContent);
                   post.child("username").setValue( dataSnapshot.child("g").getValue() ).addOnCompleteListener(new OnCompleteListener<Void>() {
                       @Override
                       public void onComplete(@NonNull Task<Void> task) {

                       }
                   });
               }

               @Override
               public void onCancelled(DatabaseError databaseError) {
                   Toast.makeText(getApplicationContext(),"Message could not be sent", Toast.LENGTH_LONG).show();

               }
           });
            messageList.scrollToPosition(messageList.getAdapter().getItemCount());
        }

    }

    @Override
    protected void onStart()
    {
        super.onStart();

        FirebaseRecyclerAdapter <Message, MessageViewHolder> FBRA = new FirebaseRecyclerAdapter<Message, MessageViewHolder>(

        Message.class,
                R.layout.singlemessagelayout,
                MessageViewHolder.class,
                mDatabase)
        {
            @Override
            protected void populateViewHolder(MessageViewHolder viewHolder, Message model, int position)
            {
                viewHolder.setContent(model.getContent());
                viewHolder.setUsername(model.getUsername());
                viewHolder.setTime(model.getTime());
            }



        };
        messageList.setAdapter(FBRA);
    }


    public static class MessageViewHolder extends RecyclerView.ViewHolder
    {
        View mView;
        public MessageViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setContent(String content)
        {
            TextView message_content = mView.findViewById(R.id.messageText);
            message_content.setText(content);
        }

        public void setUsername(String username)
        {
            TextView usernameContent = mView.findViewById(R.id.usernameText);
            usernameContent.setText(username);
        }

        public void setTime(String time)
        {
            TextView messageTime = mView.findViewById(R.id.messgageTime);
            messageTime.setText(time);

        }

    }

    public RecyclerView getMessageList()
    {
        return messageList;
    }

    public void positionsClicked(View view)
    {
        Intent positionsIntent = new Intent(ChatActivity.this, GPS.class)   ;
        startActivity(positionsIntent);
    }


}
